#lang racket/base

(require racket/function
         racket/system
         racket/match
         racket/string
         racket/dict
         racket/list
         xml)

(define/match (xmlify-arg a)
  [((? number? n)) (number->string (exact->inexact n))]
  [((? symbol? s)) (symbol->string s)]
  [((? string? s)) s]
  [((list a ...)) (string-join (map xmlify-arg a) " ")])

(define-syntax transformation
  (syntax-rules ()
    ((_ (name values ...) ...)
     `(,(format "~a(~a)" 'name (xmlify-arg (list values ...))) ...))))

(define/match (xmlify x)
  [((list tag args content ...))
   (list* tag
          (map (λ (arg)
                 (list (first arg)
                       (xmlify-arg (second arg))))
               args)
          (map xmlify content))]
  [((? string? s)) s])




(define (generate-flag-svg inside)
  `(svg ((viewBox (0 0 36 36)) (xmlns "http://www.w3.org/2000/svg"))
        (clipPath ((id "flagClip"))
                  (rect ((height 26)
                         (width 36)
                         (rx 4)
                         (ry 4))))
        (g ((clip-path "url(#flagClip)")
            (transform ,(transformation (translate 0 5))))
           ,(inside 26))))

(define (generate-heart-svg inside)
  `(svg ((xmlns "http://www.w3.org/2000/svg")  (viewBox (0 0 36 36)))
        (clipPath ((id "heartClip"))
                  ; Heart
                  (path ((d ((M 35.885 11.833)
                             (c 0 -5.45 -4.418 -9.868 -9.867 -9.868
                                -3.308 0 -6.227 1.633 -8.018 4.129
                                -1.791 -2.496 -4.71 -4.129 -8.017 -4.129
                                -5.45 0 -9.868 4.417 -9.868 9.868
                                0 .772 .098 1.52 .266 2.241)
                             (C 1.751 22.587 11.216 31.568 18 34.034)
                             (c 6.783 -2.466 16.249 -11.447 17.617 -19.959
                                .17 -.721 .268 -1.469 .268 -2.242)
                             (z))))))
        (g ((clip-path "url(#heartClip)"))
           (g ((transform ,(transformation (translate 0 2))))
              ,(inside 32)))))

(define (make-flag name content)
  (make-emoji generate-flag-svg name content))

(define (make-heart name content)
  (make-emoji generate-heart-svg name content))

(define (make-both name content)
  (make-heart (format "~a heart" name) content)
  (make-flag name content))

(define (make-emoji shape name content)
  (define svg-file (format "~a.svg" name))
  (define png-file (format "~a.png" name))
  (define flag-svg (shape content))

  (with-output-to-file svg-file #:mode 'text #:exists 'truncate
    (thunk
      (write-xml/content (xexpr->xml (xmlify flag-svg)))
      ))
  (system* "/usr/bin/env" "rsvg-convert" svg-file "-o" png-file "--width=256" "--height=256")
  name)


(define (make-vertical-stripes stripes)
  (lambda (pattern-height)
    (define total (for/sum ([(color size) (in-dict stripes)])
                    size))
    (define unit-size (/ pattern-height total))

    (define stripes-svg (reverse (for/fold ([rects '()]
                                            [height total]
                                            #:result rects)
                                           ([(color size) (in-dict (reverse stripes))])
                                   (define stripe-end (* unit-size height))
                                   (define stripe-height (* unit-size size 5/4))
                                   (define stripe-start (- stripe-end stripe-height))
                                  
                                 
                                   (values (cons `(rect ((fill ,color)
                                                         (width 36)
                                                         (y ,stripe-start)
                                                         (height ,stripe-height)))
                                                 rects)
                                           (- height size)))))

    (list* 'g '() stripes-svg)))

(define black "#222222")
(define blue "#114cc1")
(define gold "gold")
(define green "#339933")
(define light-blue "#57c0ff" #;"#3fc6fc")
(define light-pink "#fc85c2")
(define pink "#ed479c")
(define purple "#9857c1")
(define white "white")
(define yellow "#ffff44")
(define light-orange "#fc7e49")
(define orange "#ea4b07")
(define grey "#888888")
(define light-green "#7dce7d")
(define dark-blue "#032f87")
(define dark-green "#186718")
(define brown "#8B4513")
(define red "#ff0000")

(define rainbow-base `(("red" . 1)
                       ("orange" . 1)
                       (,gold . 1)
                       (,green . 1)
                       (,blue . 1)
                       (,purple . 1)))

(define flags (list
               (make-both "Pride"
                          (make-vertical-stripes rainbow-base))

               (make-both "Progress Pride"
                          (λ (h) `(g ()
                                     ,((make-vertical-stripes rainbow-base) h)
                                     ,@(map (λ (color distance)
                                              `(path ((d ((M ,(* 3.5 distance) 0)
                                                          (l ,(/ h 2) ,(/ h 2))
                                                          (l ,(/ h -2) ,(/ h 2))
                                                          (l -10 0)
                                                          (l 0 ,(- h))
                                                          (z)))
                                                      (fill ,color))))
                                            (list black brown light-blue light-pink white)
                                            '(2 1 0 -1 -2))
                                     )))
               
               (make-both "Bisexual"
                          (make-vertical-stripes `((,pink . 2)
                                                   (,purple . 1)
                                                   (,blue . 2))))

               (make-both "Non-binary"
                          (make-vertical-stripes `((,yellow . 1)
                                                   (,white . 1)
                                                   (,purple . 1)
                                                   (,black . 1))))

               (make-both "Genderqueer"
                          (make-vertical-stripes `((,purple . 1)
                                                   (,white . 1)
                                                   (,green . 1))))

               (make-both "Genderfluid"
                          (make-vertical-stripes `((,pink . 1)
                                                   (,white . 1)
                                                   (,purple . 1)
                                                   (,black . 1)
                                                   (,blue . 1))))

               (make-both "Pansexual"
                          (make-vertical-stripes `((,pink . 1)
                                                   (,gold . 1)
                                                   (,light-blue . 1))))

               (make-both "Transgender"
                          (make-vertical-stripes `((,light-blue . 1)
                                                   (,light-pink . 1)
                                                   (,white . 1)
                                                   (,light-pink . 1)
                                                   (,light-blue . 1))))

               (make-both "Lesbian"
                          (make-vertical-stripes `((,orange . 1)
                                                   (,light-orange . 1)
                                                   (,white . 1)
                                                   (,light-pink . 1)
                                                   (,pink . 1))))

               (make-both "Asexual"
                          (make-vertical-stripes `((,black . 1)
                                                   (,grey . 1)
                                                   (,white . 1)
                                                   (,purple . 1))))

               (make-both "Aromantic"
                          (make-vertical-stripes `((,green . 1)
                                                   (,light-green . 1)
                                                   (,white . 1)
                                                   (,grey . 1)
                                                   (,black . 1))))

               (make-both "Intersex"
                          (λ (h) `(g ()
                                     ,((make-vertical-stripes `((,gold . 1))) h)
                                     (circle ((cx 18)
                                              (cy ,(/ h 2))
                                              (r 8)
                                              (fill "none")
                                              (stroke ,purple)
                                              (stroke-width 3))))))

               (make-both "MLM"
                          (make-vertical-stripes `(("#339977" . 1)
                                                   (,light-green . 1)
                                                   (,white . 1)
                                                   (,light-blue . 1)
                                                   (,blue . 1))))

               (make-both "Agender"
                          (make-vertical-stripes `((,black . 1)
                                                   (,grey . 1)
                                                   (,white . 1)
                                                   ("#4fea4f" . 1)
                                                   (,white . 1)
                                                   (,grey . 1)
                                                   (,black . 1))))

               (make-both "Cinthean"
                          (make-vertical-stripes `(("#008" . 1)
                                                   ("#80a" . 1)
                                                   ("#f48" . 1)
                                                   ("#f88" . 1)
                                                   ("#fa8" . 1)
                                                   ("#fff" . 1))))

               (make-both "Polyamory" ; Based on https://www.polyamproud.com/flag
                          (λ (h) `(g ()
                                     ,((make-vertical-stripes `(("#009FE3" . 1) ("#E50051" . 1) ("#340C46" . 1))) h)
                                     ,@(map (λ (color distance)
                                              `(path ((d ((M ,(* 3.5 distance) 0)
                                                          (l ,(- (/ h 2) (* h 1/6)) ,(- (/ h 2) (* h 1/6)))
                                                          (l ,(+ (/ h -2) (* h -1/6)) ,(+ (/ h 2) (* h 1/6)))
                                                          (l -10 0)
                                                          (l 0 ,(- h))
                                                          (z)))
                                                      (fill ,color))))
                                            (list white)
                                            (list 2.5))
                                     (path ((d ((M ,(* h 18/36) ,(* h 1/3))
                                                (l -4 -4)
                                                (a 2 2 "0" "0" "0" -4 4)
                                                (a 2 2 "0" "0" "0" 4 4)
                                                (z)))
                                            (fill "#FCBF00")))
                                     )))

               #;(make-both "Mango"
                          (make-vertical-stripes `(("#ff81a8" . 1)
                                                   ("#ffa18f" . 1)
                                                   ("#ffb67d" . 1)
                                                   ("#d1c87d" . 1)
                                                   ("#8fd58c" . 1)
                                                   ("#03d2a0" . 1)
                                                   )))
               ))

(with-output-to-file "index.html" #:mode 'text #:exists 'truncate
  (thunk
    (write-xml/content (xexpr->xml (xmlify
                                    `(html ()
                                           (body ()
                                                 (a ((href "flags.zip") (style "font-size:1.5em")) "Download all .png")
                                                 (div ((style "display:flex;flex-direction:row;flex-wrap:wrap;"))
                                                      ,@(map (λ (flag)
                                                               `(div ((style "margin:10px"))
                                                                     ;(p () ,flag)
                                                                     (img ((width 128)
                                                                           (alt ,flag)
                                                                           (title ,flag)
                                                                           (src ,(format "~a.svg" flag))))))
                                                             flags))
                                                 (a ((href "https://gitlab.com/pythondude325/pride-flag-emojis")) "Source code")))
                                    )))))